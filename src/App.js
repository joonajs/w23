import Reporter from './Reporter';
import Morko from './morko.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <img className='morko' src={Morko} alt="Mörkö" />
<Reporter name="Antero Mertaranta">Löikö mörkö sisään</Reporter>
<Reporter name="Kevin McGran">I know it's a rough time now, but did you at least enjoy playing in the tournament</Reporter>
    </div> 
  );
}

export default App;
